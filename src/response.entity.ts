import { Column,Entity,PrimaryGeneratedColumn} from "typeorm";



@Entity('responses')
export class Responses {
    @PrimaryGeneratedColumn()
    code: number;

    @Column()
    status: string;

    @Column()
    message: string;

    @Column()
    data:any;

}
