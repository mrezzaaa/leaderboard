import { Controller, Get, Post, Body, Param, HttpStatus, HttpException  } from '@nestjs/common';
import { Responses } from 'src/response.entity';
import { Users } from './users.entity';
import * as md5 from 'md5'

import { UsersService } from './users.service';
@Controller('users')
export class UsersController {

    constructor(private readonly userService : UsersService){

    }

    @Get()
    async renderIndex(){
        let result = await this.userService.findAll()
        let response = new Responses()
        result.map((r)=> delete r.password)
        response.code = 200
        response.status = "success"
        response.data = result
        response.message = `${response.data.length} object`;
        return response;
    }

    @Get(":id")
    async handleUsingId(@Param("id") id:number){
        let result = await this.userService.findById(id);
        if(result?.id){
            let response = new Responses()
            delete result.password;
            response.code = 201
            response.status = "success"
            response.data = result
            response.message = `${Object.keys(result).length} object`
            return response
        }
        else{
            throw new HttpException('No user found', HttpStatus.NOT_ACCEPTABLE);
        }
    }

    @Post("register")
    async handlePost(@Body() body){
        let newuser = new Users();
        newuser.username = body.username;
        newuser.password = md5(body.password);
        let insert:any = await this.userService.createUser(newuser.username,newuser.password);
        if(insert?.message){
            throw new HttpException(insert.message, HttpStatus.CONFLICT);
        }
        else{
            let response = new Responses()
            response.code = 200
            response.status =  "success"
            response.data = insert
            response.message = `${Object.keys(insert).length} object`
            return response
        }

    }
}
