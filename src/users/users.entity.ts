import { Column,Entity,OneToMany,PrimaryGeneratedColumn} from "typeorm";
import { Score } from "src/score/score.entity";


@Entity('users')
export class Users {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    username: string;

    @Column()
    password: string;

    @OneToMany(() => Score, score => score.user)
    scores:Score[];

}
