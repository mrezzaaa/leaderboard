import { Entity, Repository } from 'typeorm';
import { Users } from 'src/users/users.entity';

@Entity("users")
export class UsersRepository extends Repository<Users> {}
