import { Module } from '@nestjs/common';
import { JwtModule, JwtService } from '@nestjs/jwt';
import { PassportModule } from '@nestjs/passport';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AuthService } from './auth.service';
import { UsersService } from 'src/users/users.service';
import { JwtStrategy } from './jwt.strategy';
import { UsersRepository } from './users.repository';
import { Users } from 'src/users/users.entity';
import { AuthController } from './auth.controller';

@Module({
  imports: [
  TypeOrmModule.forFeature([Users, UsersRepository]),
    PassportModule.register({ defaultStrategy: 'jwt' }),
    JwtModule.register({
      secret: 's3cr3tk3y',
      signOptions: { expiresIn: '60s' },
    }),
  ],
  providers: [AuthService, UsersService, JwtStrategy,JwtService],
  controllers: [AuthController],
  exports: [AuthService, UsersService],
})
export class AuthModule {}
