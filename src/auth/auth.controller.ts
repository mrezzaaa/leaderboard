import { Controller,Get,Post,Param,Body } from '@nestjs/common';
import { UsersService } from 'src/users/users.service';
import { JwtStrategy } from './jwt.strategy';
import * as md5 from 'md5'
import { AuthService } from './auth.service';
import { JwtPayload } from './jwt-payload.interface';
import { JwtService } from '@nestjs/jwt';

@Controller('auth')
export class AuthController {
    constructor(private readonly userService:UsersService,private readonly authService:AuthService,private readonly jwt:JwtService){

    }

    @Post()
    loggingin(@Body() data:any){
        console.log(data.username,data.password)
        
        return this.userService.findByUsernamePassword(data.username,md5(data.password)).then(async (result)=>{
            let user = result[0]
            const payload: JwtPayload = {username : user.username,sub:user.id}
            return user
            // let access_token = await this.jwt.signAsync(payload)
            // return access_token
        })
    }
}
