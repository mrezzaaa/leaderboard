import { MiddlewareConsumer, Module, NestModule } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AuthModule } from './auth/auth.module';
import { ScoreModule } from './score/score.module';
import { Users } from './users/users.entity';
import { Score } from './score/score.entity';
import { UsersModule } from './users/users.module';
import { AppController } from './app.controller';
import { UsersController } from './users/users.controller';
import { ScoresController } from './score/score.controller';
import { AuthController } from './auth/auth.controller';
import { AppService } from './app.service';
import { UsersService } from './users/users.service';
import { ScoreService } from './score/score.service';
import { AuthService } from './auth/auth.service';
import { JwtStrategy } from './auth/jwt.strategy';
import { JwtService } from '@nestjs/jwt';
import { LoggerMiddleware } from './logging/logging.middleware';
import { DataSource } from 'typeorm';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'postgres',
      host: 'localhost',
      port: 5432,
      username: 'icewalker',
      password: '',
      database: 'leaderboard',
      entities: [Users, Score],
      synchronize: true,
    }),
    AuthModule,
    UsersModule,
    ScoreModule,
  ],
  controllers: [AppController, UsersController, ScoresController, AuthController],
  providers: [AppService, UsersService, ScoreService, AuthService,JwtService],
})
export class AppModule implements NestModule {
  constructor(private dataSource: DataSource) {}
  configure(consumer: MiddlewareConsumer) {
    consumer.apply(LoggerMiddleware).forRoutes('*');
  }
}
