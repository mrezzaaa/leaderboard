import { Column,Entity,OneToMany,ManyToOne,PrimaryGeneratedColumn,JoinColumn} from "typeorm";
import { Users } from "src/users/users.entity";

@Entity('scores')
export class Score {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    userid: number;

    @Column()
    score: number;

    @ManyToOne(() => Users, users => users.scores)
    @JoinColumn({ name: 'userid' })
    user: Users;

}
