import { Controller, Post, Body, Get, UseGuards, Req } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ScoreService } from './score.service';
import { CreateScoreDto } from './dto/create-score.dto';
import { Users } from 'src/users/users.entity';

@Controller('score')
export class ScoresController {
  constructor(private readonly scoresService: ScoreService) {}

  // @UseGuards(AuthGuard('jwt'))
  @Post()
  async handlePost(@Body() data:any) {
    return this.scoresService.insertOne(data.userid, data.score);
  }

  @Get('leaderboard')
  async findTop10() {
    let result = await this.scoresService.findTop10();
    result.map((r)=> delete r.user.password)
    return result
  }
}
